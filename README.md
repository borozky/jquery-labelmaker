## LabelMaker

Written by Joshua Orozco (borozky@gmail.com)

Made using:
- create-react-app (React is removed)
- ES6
- jQuery
- Redux
- FileSaver.js
- JsBarcode

How to run:
1. Clone this repo. Navigate to folder <code>cd jquery-labelmaker</code>
2. Install dependencies <code>npm install</code>
3. Run the app using <code>npm start</code>